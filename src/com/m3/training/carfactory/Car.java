package com.m3.training.carfactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;

public abstract class Car implements ICar {

	@Override
	public void drive() {
		System.out.println("driving my car");
	}

	@Override
	public void refuel() {
		System.out.println("receiving fuel");
	}

	// not naive, but not optimal
	public static ICar createCar(String make) {
		ICar icar = null;

		if (make.compareTo("MercedesBenz") == 0) {
			int luxuryLevel = 9000;
			icar = new MercedesBenz(luxuryLevel);
		}

		if (make.compareTo("Ford") == 0) {
			String model = "Mustang";
			icar = new Ford(model);
		}
		return icar;
	}

	public static ICar createCarReflection(String make) {
		ICar icar = null;
		String fullName = Car.class.getPackageName() + "." + make;
		try {
			// returns the Class object for the class with the specified name
			Class<?> cls = Class.forName(fullName);
			Constructor<?>[] constructors = cls.getDeclaredConstructors();
			if (constructors.length < 1) {
				// throw something
				// don't fail silently
				return icar;
			}
			Constructor<?> constructor = constructors[0];
			Parameter[] parameters = constructor.getParameters();
			Object[] initargs = new Object[parameters.length];
			for (int index = 0; index < parameters.length; index++) {
				if (parameters[index].getType().getName().compareTo("int") == 0) {
					initargs[index] = 0;
				}
				if (parameters[index].getType().getName().compareTo("String") == 0) {
					initargs[index] = "";
				}
			}
			icar = (ICar) constructor.newInstance(initargs);
			
		// don't just consume these errors by dumping a stack trace
			// let your user know that the creation was a failure
		} catch (ClassNotFoundException ex) {
			System.out.println(ex.toString());
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return icar;
	}

}
