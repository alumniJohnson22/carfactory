package com.m3.training.carfactory;

public class Ford extends Car {
	
	private String model;

	public Ford(String model) {
		this.model = model;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}
	
}
