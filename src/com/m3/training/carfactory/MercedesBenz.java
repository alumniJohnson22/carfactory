package com.m3.training.carfactory;

public class MercedesBenz extends Car {
	
	private int luxuryLevel;

	public int getLuxuryLevel() {
		return luxuryLevel;
	}

	public void setLuxuryLevel(int luxuryLevel) {
		this.luxuryLevel = luxuryLevel;
	}

	public MercedesBenz(int luxuryLevel) {
		this.luxuryLevel = luxuryLevel;
	}

}
