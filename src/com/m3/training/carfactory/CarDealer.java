package com.m3.training.carfactory;

public class CarDealer {
	
	public static void main(String[] args) {
		// naive
		int luxuryLevel = 9000;
		MercedesBenz mercy = new MercedesBenz(luxuryLevel);
		String model = "Mustang";
		Ford bumbleBee = new Ford(model);
		Car car = mercy;
		car = bumbleBee;
		ICar icar = car;
		icar = mercy;
		
		// Obscure the detail of constructors: Why?
		// details are complicated
		// details are trade secrets
		// decision to invoke a constructor taken away from user
		
		// simple car factory
		icar = Car.createCar("Ford");
		System.out.println(icar.getClass());
		icar = Car.createCarReflection("MercedesBenz");
		icar.drive();
		icar.refuel();
		if (icar instanceof MercedesBenz) {
			((MercedesBenz) icar).setLuxuryLevel(20);
			System.out.println("Luxury level is " + ((MercedesBenz) icar).getLuxuryLevel());
		}
		System.out.println(icar.getClass());
		icar = Car.createCarReflection("Mazda");
		System.out.println(icar);
		/*
		 * Two reasons not to support Telsa
		 * Factory does not understand Telsa
		 * Car heirarchy does not model electric
		icar = Car.createCar("Telsa");
		System.out.println(icar.getClass());		
		*/
		
		
		
		
		
		
		
		
		
	}
}
