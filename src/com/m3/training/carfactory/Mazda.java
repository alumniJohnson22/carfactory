package com.m3.training.carfactory;

public class Mazda extends Car {
	
	private String color;
	private int cylinders;

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getCylinders() {
		return cylinders;
	}

	public void setCylinders(int cylinders) {
		this.cylinders = cylinders;
	}

	public Mazda(String color, int cylinders) {
		if (color == null) {
			color = "Blue";
		}
		this.color = color;
		if (cylinders == 0) {
			cylinders = 4;
		}
		this.cylinders = cylinders;
	}
	
	public String toString() {
		return super.toString() + " cylinders are " + this.getCylinders() + " color is " + this.getColor();
	}

}
