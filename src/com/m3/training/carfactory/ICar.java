package com.m3.training.carfactory;

public interface ICar {

	void drive();

	void refuel();

}